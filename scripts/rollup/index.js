const { rollup, watch } = require('rollup');
const babel = require('rollup-plugin-babel');
const uglify = require('rollup-plugin-uglify');

const buildMode = process.env.NODE_ENV;

// const EVENTS = {
//   START: 'START',
//   FATAL: 'FATAL',
// };

const getInputOptions = (mode) => {
  switch (mode) {
    case 'prod':
      return {
        input: 'src/index.js',
        plugins: [
          babel(),
          uglify(),
        ],
      };
    case 'dev':
    default:
      return { input: 'src/index.js' };
  }
};
const getOutputOptions = (mode) => {
  switch (mode) {
    case 'prod':
    case 'dev':
    default:
      return {
        file: 'dist/index.js',
        format: 'es',
      };
  }
};

const inputOptions = getInputOptions(buildMode);
const outputOptions = getOutputOptions(buildMode);
const watchOptions = {
  ...inputOptions,
  output: [outputOptions],
  watch: { chokidar: false },
};

async function build() {
  const bundle = await rollup(inputOptions);
  await bundle.write(outputOptions);
}

if (buildMode === 'dev') {
  const watcher = watch(watchOptions); // returns a watcher

  watcher.on('event', (event) => {
    console.log(event);
  });
} else {
  build();
}
