const serve = require('serve');
const path = require('path');

serve(path.resolve('dist'), {
  port: 1337,
  ignore: ['node_modules'],
});
