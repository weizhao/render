module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "import/no-extraneous-dependencies": ["error", { "devDependencies": true }],
    "import/prefer-default-export": "off",
    "object-curly-newline": ["error", { "multiline": true, "minProperties": 3 }],
    "function-paren-newline": ["error", "consistent"]
  },
  "env": {
    "browser": true,
    "node": true
  }
};