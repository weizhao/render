import {
  INCREMNT,
  DECREMENT,
} from '../constants/actionTypes';

const counter = state => (action = {}) => {
  const { type } = action;

  switch (type) {
    case INCREMNT:
      return { counter: state.counter + 1 };
    case DECREMENT:
      return { counter: state.counter - 1 };
    default:
      return state;
  }
};

export default counter;
