import {
  h1,
  text,
  header,
} from '../core/h';

const headerView = () => header()(
  h1()(text('Simple Counter')),
);

export default headerView;
