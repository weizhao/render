import {
  text,
  ul, li,
  footer,
  listOf,
} from '../core/h';

const list = listOf(li)();

const footerView = () => footer()(
  ul()(
    ...list(
      text('about'),
      text('readme'),
      text('gitlab'),
    ),
  ),
);

export default footerView;
