import compose from '../core/utils/compose';
import { onClick } from '../core/events';
import { increment, decrement } from '../actions/counter';
import {
  div,
  button,
  p,
  text,
} from '../core/h';

const displayView = compose(text, p(), div());

const buttonView = action => label => button(
  onClick(action),
)(text(label));

const mainView = ({ counter }) => div()(
  buttonView(increment)('+'),
  displayView(counter), // <------ needs update
  buttonView(decrement)('-'),
);

export default mainView;
