import { div } from '../core/h';

import headerView from './header';
import mainView from './main';
import footerView from './footer';

const app = ({ counter }) => div()(
  headerView(),
  mainView({ counter }),
  footerView(),
);

export default app;
