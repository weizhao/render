import {
  INCREMNT,
  DECREMENT,
} from '../constants/actionTypes';
// action creators
export const increment = () => ({ type: INCREMNT });
export const decrement = () => ({ type: DECREMENT });
