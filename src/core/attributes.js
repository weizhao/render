export const EVENT = 'event';
export const STYLE = 'style';
export const ATTR = 'attr';

const getAttributes = type => (...attributes) => (
  attributes.filter(attr => (attr.type === type))
);

export const getEvents = getAttributes(EVENT);
export const getStyles = getAttributes(STYLE);
export const getAttrs = getAttributes(ATTR);
