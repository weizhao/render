const compose = (fn, ...funcs) => (...args) => (
  funcs.reduce((acc, func) => func(acc), fn(...args))
);

export default compose;
