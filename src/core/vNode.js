const vText = (content = '') => (
  { text: content.toString() }
);

const vNode = (nodeName = '') => (...attributes) => (...children) => ({
  nodeName,
  attributes,
  children,
});

const vVoidNode = (nodeName = '') => (...attributes) => ({
  nodeName,
  attributes,
});

const vListOf = h => (...attributes) => (...children) => {
  if (typeof h !== 'function') {
    throw new Error('not a function');
  }

  return children.map(child => h(...attributes)(child));
};

export {
  vNode,
  vVoidNode,
  vText,
  vListOf,
};
