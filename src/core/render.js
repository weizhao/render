import applyEvents from './applyEvents';

const createElement = (vNode) => {
  if (vNode.text) {
    return document.createTextNode(vNode.text);
  }
  return document.createElement(vNode.nodeName);
};

const render = vNode => (parent) => {
  const domElement = createElement(vNode);

  if (vNode.attributes) {
    applyEvents(domElement)(...vNode.attributes);
  }

  if (vNode.children) {
    vNode.children.forEach(child => render(child)(domElement));
  }

  return parent.appendChild(domElement);
};

export default render;
