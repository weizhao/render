import { EVENT } from './attributes';

const on = eventName => actionCreator => ({
  type: EVENT,
  eventName,
  actionCreator,
});

const onClick = on('click');

const onDoubleClick = on('dbclick');

const onMouseDown = on('mouseDown');

const onMouseUp = on('mouseUp');

const onMouseOver = on('mouseOver');

const onMouseOut = on('mouseOut');

export {
  on,
  onClick, onDoubleClick,
  onMouseDown, onMouseUp,
  onMouseOver, onMouseOut,
};
