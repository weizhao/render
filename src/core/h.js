import {
  vNode,
  vVoidNode,
  vText,
  vListOf,
} from './vNode';

const node = vNode;

const text = vText;

const listOf = vListOf;

const header = vNode('header');

const footer = vNode('footer');

const div = vNode('div');

const span = vNode('span');

const p = vNode('p');

const img = vVoidNode('img');

const button = vNode('button');

const ul = vNode('ul');

const ol = vNode('ol');

const li = vNode('li');

const h1 = vNode('h1');

const h2 = vNode('h2');

const h3 = vNode('h3');

const h4 = vNode('h4');

const h5 = vNode('h5');

const h6 = vNode('h6');

const small = vNode('small');

export {
  node, text, listOf,
  div, p,
  header, footer,
  span,
  img,
  button,
  ul, ol, li,
  h1, h2, h3, h4, h5, h6,
  small,
};
