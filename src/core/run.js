import render from './render';
import middleware from './middleware';
import compose from './utils/compose';

// initial run
// -> get state
// -> (create view -> create v doms -> apply attributes)
// -> render

// update content
// -> get state
// -> (create view -> create v doms -> apply attributes)
// -> render

// update tree (reapply features, styles)
// -> get state
// -> compare old v doms tree with new v doms tree
// -> add path functon to v node
// -> apply patches to the old dom tree

export const run = app => ({
  with: state => ({
    use: (...middlewares) => ({
      in: (domElement) => {
        // combine all the middlewares
        const reducers = compose(middleware, ...middlewares);
        // get initial state
        const appState = reducers(state)();
        const view = app(appState);

        render(view)(domElement);
      },
    }),
  }),
});
