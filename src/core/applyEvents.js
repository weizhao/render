import { getEvents } from './attributes';

const applyEvents = domNode => (...attributes) => {
  const events = getEvents(...attributes);

  events.forEach((event) => {
    const { eventName, actionCreator } = event;

    domNode.addEventListener(eventName, (e) => {
      const action = actionCreator(e);
      // TODO: use golobal event delegator
      console.log(action);
    }, false);
  });

  // TODO: handle custom events

  return domNode;
};

export default applyEvents;
