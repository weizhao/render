import { run } from './core/run';
import app from './views/app';
import counter from './middlewares/counter';

run(app) // app view
  .with({ counter: 0 }) // initial state
  .use(counter) // middlewares, for example: .use(counter, logger, otherMiddleware)
  .in(document.body); // placehoder dom element
